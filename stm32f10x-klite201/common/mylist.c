/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include "mylist.h"

static void __list_add(node_s *node, node_s *prev, node_s *next)
{
	next->m_prev = node;
	node->m_next = next;
	node->m_prev = prev;
	prev->m_next = node;
}
static void __list_del(node_s *prev, node_s *next)
{
	next->m_prev = prev;
	prev->m_next = next;
}
static void __list_del_entry(node_s *entry)
{
	__list_del(entry->m_prev, entry->m_next);
}

void list_add(node_s *node, list_s *head)
{
    link_s *iter=NULL;
    if (node==NULL || head==NULL)
    {
        return;
    }
    /******************************************
     * 在往链表添加节点前先检查是否有同样的结点
     * 不允许加入一样的结点,否则链表会出问题.
     *****************************************/
    if(node==head) 
    {
        TRACE_ERR("list_add: same node!");
        return;
    }
    LIST_FOR_OTHERS(iter, head)
    {
        if(node==iter) 
        {
            TRACE_ERR("list_add: same node!");
            return;
        }
    }
    
    __list_add(node, head, head->m_next);
}

void list_add_tail(node_s *node, list_s *head)
{
    link_s *iter=NULL;
    if (node==NULL || head==NULL)
    {
        return;
    }
    /******************************************
     * 在往链表添加节点前先检查是否有同样的结点
     * 不允许加入一样的结点,否则链表会出问题.
     *****************************************/
    if(node==head) 
    {
        TRACE_ERR("list_add: same node!");
        return;
    }
    LIST_FOR_OTHERS(iter, head)
    {
        if(node==iter) 
        {
            TRACE_ERR("list_add: same node!");
            return;
        }
    }
    
    __list_add(node, head->m_prev, head);
}


void list_del(node_s *entry)
{
    if(entry==NULL)
    {
        return;
    }
    /*****************************************
     * 不允许删除最后一个结点,否则将无法判断
     * 链表是否为空,因为此时前后结点都为NULL
     ******************************************/
     if (list_is_empty(entry))
     {
        TRACE_ERR("list_del: empty!");
        return;
     }
     
	__list_del(entry->m_prev, entry->m_next);
    entry->m_prev = NULL;
    entry->m_next = NULL;
}

/********************************************
 * 空链表表示只有一个结点,该结点前后结点指针
 * 都指向自己.
 ********************************************/
bool list_is_empty(const list_s *head)
{
    if(head==NULL)
    {
        return false;
    }
    return head->m_next == head;
}

