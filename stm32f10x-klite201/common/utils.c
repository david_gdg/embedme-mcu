/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "utils.h"
#include <string.h>

bool utils_ishexchar(char ch)
{
    if(ch>='0' && ch<='9')
        return true;
    else if(ch>='A' && ch<='F')
        return true;
    else 
        return false;
}

/* 16进制字符转换为对应的字面值('A'->0x0A) */
sint8 utils_hexchar2val(char ch)
{
	if(ch>='a' && ch <='f')
	{
		return (ch-'a'+0x0A);
	}
	else if(ch>='A' && ch <='F')
	{
		return (ch-'A'+0x0A);
	}
	else if(ch>='0' && ch<='9')
	{
		return (ch-'0');
	}
	else
	{
		return -1;
	}
}

/* 16进制字符串转换为对应的字面值('FFFF'->FFFF) */
uint32 utils_hexstr2bytes(char* hexString,char* bytes,uint32 size)
{
    uint32 i;
    uint32 hexLen = strlen(hexString);
    uint32 len = hexLen>>1;
    if(hexLen%2!=0)
    {
        return 0;
    }
    for(i=0; (i<len)&&(i<size) ;i++)
    {
        sint8 high = utils_hexchar2val(hexString[i<<1]);
        sint8 low = utils_hexchar2val(hexString[(i<<1)+1]);
        if(high==-1 || low==-1)
        {
            break;
        }
        bytes[i]=(high<<4)+low; 
    }
    return i;
}

