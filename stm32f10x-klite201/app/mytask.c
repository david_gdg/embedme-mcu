/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "mytask.h"
#include "basetype.h"
#include "trace.h"
#include "board.h"
#include "uart.h"
#include "gpio.h"
#include "rtc.h"
#include "button.h"
#include "mytimer.h"

/* 模块测试 */
#define TEST_MODULE  0  /* 测试模块打开时需要在Keil工程中加入test_module.c文件 */
#if TEST_MODULE
extern void test_module(void);
#endif

#define SHELL_MAX_ARGC  6
/*******************************************************
 * shell_excute:
 * 执行shell命令(当前支持最大6个参数)
 ******************************************************/
void shell_excute(char** argv,uint8 argc)
{
    if(argc==1 && strcmp(argv[0],"help")==0)
    {
        TRACE_YELLOW("command list:");
        TRACE_YELLOW("test     -->工厂测试.");
        TRACE_YELLOW("free     -->堆栈信息.");
        TRACE_YELLOW("date     -->查看日期时间.");
        TRACE_YELLOW("uname    -->查看系统信息.");
    }
    else if(argc==1 && strcmp(argv[0],"test")==0)
    {
    }
    else if(argc==1 && strcmp(argv[0],"free")==0)
    {
        uint32 total,used;
        kmem_info(&total,&used);
        TRACE_YELLOW("kernel memory total:%d bytes, used:%d bytes, usaged:%d%%.",total,used,used*100/total);
    }
    else if (argc==1 && strcmp(argv[0],"date")==0)
    {
        Time_S time={0};
        rtc_gettime(&time);
        TRACE_YELLOW("%04d-%02d-%02d W%d %02d:%02d:%02d",time.m_year,time.m_month,time.m_date,time.m_weekday+1,time.m_hour,time.m_minute,time.m_second);
    }
    else if (argc==1 && strcmp(argv[0],"uname")==0)
    {
        TRACE_YELLOW("version: %s (%s %s)",APP_VERSION,__DATE__,__TIME__);
        TRACE_YELLOW("OS TYPE: %s V%x",OS_NAME,kernel_version());
    }
    else if (argc==1 && strcmp(argv[0],"reboot")==0)
    {
        system_reboot();
    }
    else if(argc==2 && strcmp(argv[0],"trace")==0)
    {
        uint8 level = atoi(argv[1]);
        level=CLIP(1,level,5);
        TRACE_LEVEL(level);
    }
    else if(argc>=4 && strcmp(argv[0],"gpio")==0) /* gpio设置/获取命令 */
    {
        uint32 gpio;
        char group = *(argv[2]);
        uint8 pin = atoi(argv[3]);
        switch(group){
        case 'A':
            gpio = PA(pin);
            break;
        case 'B':
            gpio = PB(pin);
            break;
        case 'C':
            gpio = PC(pin);
            break;
        case 'D':
            gpio = PD(pin);
            break;
        default:
            TRACE_RED("Not support group %c!",group);
            return;
        }
        if(strcmp(argv[1],"set")==0 && argc==5)
        {
            uint8 value= atoi(argv[4]);
            GPIO_INIT(gpio,GPIO_MODE_OUT_PP,GPIO_SPEED_50MHZ);
            GPIO_SET_VAL(gpio,value);
            TRACE_YELLOW("gpio set %c:%d %d",group,pin,value);
        }
        else if(strcmp(argv[1],"get")==0 && argc==4)
        {
            GPIO_INIT(gpio,GPIO_MODE_IN_FLOATING,GPIO_SPEED_50MHZ);
            TRACE_YELLOW("gpio %c:%d value=%d",group,pin,GPIO_GET_VAL(gpio));
        }
    }
    else
    {
        TRACE_RED("invalid command!");
    }
}


static void console_init(void)
{
    uart_init(COM_DEBUG,COM_DEBUG_BAUD);
}

static void business_start(void)
{
    board_init();           /* 板级初始化 */
    rtc_reset();
    TRACE_GREEN("\r\n\r\n");
    TRACE_GREEN("=================================");
    TRACE_GREEN("version: %s",APP_VERSION);
    TRACE_GREEN("buildTM: %s %s",__DATE__,__TIME__);
    TRACE_GREEN("=================================");    
    while(1)
    {
        uint8 argc=0;
        char* argv[SHELL_MAX_ARGC]={0};
        argc = system_shell_read(COM_DEBUG,argv,SHELL_MAX_ARGC,'$');
        if(argc>0)
        {
            shell_excute(argv,argc);
        }
        msleep(10);
    }
}

/*******************************************************
 * task_main:
 * 主任务,系统的第一个任务
 ******************************************************/
void task_main(void *pdata)
{
    UNUSED_PARAM(pdata);
    console_init();/* 终端初始化 */
    #if APP_DEBUG
    TRACE_LEVEL(TRACE_LEVEL_DBG);/* 打开调试信息 */
    #endif
    #if USE_UART_TX_INT
    kthread_create(task_uart_tx_polling,0,0);
    #endif
    /* 定时器初始化 */
    kthread_create(task_timer_do_tick,0,0);
    #if TEST_MODULE
    test_module();/* !!! test_module()函数是个死循环,之后的代码不会运行 !!! */
    #else
    business_start();
    #endif
}

