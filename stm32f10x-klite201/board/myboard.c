/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "board.h"
#include "myboard.h"
#include "gpio.h"
#include "button.h"
#include "trace.h"

static GpioInfo_S s_myBoardGpios[GPIO_MAX]={
    {PB(7), GPIO_MODE_IPU, GPIO_SPEED_10MHZ, 0},    /* 0: GPIO_KEY_K1 */
    {PB(6), GPIO_MODE_IPU, GPIO_SPEED_10MHZ, 0},    /* 1: GPIO_KEY_K2 */
};

void board_gpio_set(uint8 gpioID, bool enable)
{
    GpioInfo_S* gpios = s_myBoardGpios;
    if(gpioID<GPIO_MAX)
    {
        GPIO_SET_VAL(gpios[gpioID].m_gpio,(enable)?(gpios[gpioID].m_enVal):(!gpios[gpioID].m_enVal));
    }
}

sint8 board_gpio_get(uint8 gpioID)
{
    GpioInfo_S* gpios = s_myBoardGpios;
    if(gpioID<GPIO_MAX)
    {
        return GPIO_GET_VAL(gpios[gpioID].m_gpio);
    }
    return -1;
}

void board_gpio_init(void)
{
    /* 由于PB3,PB4,PA13,PA14,PA15这五个IO口默认是JTAG引脚,而我们只将其当做普通IO
     * 使用,所以需要改变管脚的映射，以禁用JTAG功能,否则没法控制这些IO口.
     * GPIO_Remap_SWJ_Disable:完全禁用JTAG_DP和SW_DP
     * GPIO_Remap_SWJ_JTAGDisable:JTAG_DP禁用,SW_DP使能
     */
    uint8 i;
    GpioInfo_S* gpios = s_myBoardGpios;
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
    for(i=0; i<GPIO_MAX; i++)
    {
        GPIO_INIT(gpios[i].m_gpio,gpios[i].m_mode,gpios[i].m_speed);
    }
}

void board_init(void)
{    
    board_gpio_init();
    button_init(BTN_ID_KEY1, s_myBoardGpios[GPIO_KEY_K1]);
    button_init(BTN_ID_KEY2, s_myBoardGpios[GPIO_KEY_K2]);
}