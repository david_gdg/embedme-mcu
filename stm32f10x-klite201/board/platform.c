/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "platform.h"
#include "stm32f10x_conf.h"
#include "system_stm32f10x.h"
#include "core_cm3.h"
#include "stm32f10x.h"

/**********************************************************
 * 自己实现的系统时钟配置函数
 * @hclkdiv:    AHB时钟hclk分频系数,如RCC_SYSCLK_Div1
 * @pclk2div:   高速时钟APB2分频系数,如RCC_HCLK_Div1
 * @pclk1div:   低速时钟APB1分频系数,如RCC_HCLK_Div2
 * @pllsrc:     PLL时钟源,如RCC_PLLSource_HSE_Div1
 * @pllmul:     PLL时钟源倍频系数,如RCC_PLLMul_9
 *********************************************************/
void stm32_sysclk_init(uint32 hclkdiv,uint32 pclk2div, uint32 pclk1div, uint32 pllsrc, uint32 pllmul)
{
    /* 将外设RCC寄存器重设为缺省值 */
    RCC_DeInit();

    /* 打开外部高速晶振 */
    RCC_HSEConfig(RCC_HSE_ON);

    /* 等待HSE起振就绪 */
    if (RCC_WaitForHSEStartUp()==SUCCESS)
    {
        /* 设置AHB时钟HCLK为系统时钟,AHB时钟=系统时钟 */
        RCC_HCLKConfig(hclkdiv);

        /* 设置高速AHB时钟PCLK2,APB2时钟=HCLK */
        RCC_PCLK2Config(pclk2div);

        /* 设置低速AHB时钟PCLK2,APB1时钟=HCLK/2 */
        RCC_PCLK1Config(pclk1div);

        /* 设置PLL时钟源及倍频系数---更改倍频系数可以调整系统时钟 */
        RCC_PLLConfig(pllsrc, pllmul);

        /* 使能PLL */
        RCC_PLLCmd(ENABLE);

        /* 检查PLL是否已准备好 */
        while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY)==RESET);

        /* 设置系统时钟 */
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

        /* 0x08:PLL作为系统时钟 */
        while(RCC_GetSYSCLKSource() != 0x08);
        
    }
}

uint32 stm32_sysclk_get(void)
{
    RCC_ClocksTypeDef  rcc_clocks;
    RCC_GetClocksFreq(&rcc_clocks);
    return ((uint32)rcc_clocks.HCLK_Frequency);
}

/**********************************************************
 * 外部中断配置
 * @extiline:   外部中断线,如EXTI_Line5
 * @mode:       中断模式,如EXTI_Mode_Interrupt
 * @trigger:    中断触发方式,如EXTI_Trigger_Falling
 * @portsrc:    中断线端口,如GPIO_PortSourceGPIOC
 * @pinsrc:     中断线Pin,如GPIO_PinSource5
 *********************************************************/
void stm32_exti_init(uint32 extiline,uint8 mode, uint8 trigger,uint8 portsrc,uint8 pinsrc)
{
    EXTI_InitTypeDef EXTI_InitStructure;
    /* 先清除中断挂起位,清除旧的中断 */
    EXTI_ClearITPendingBit(extiline);

    /* 配置pinsrc为外部中断线 */
    GPIO_EXTILineConfig(portsrc, pinsrc);

    /* 配置中断 */
    EXTI_InitStructure.EXTI_Line = extiline;
    EXTI_InitStructure.EXTI_Mode = mode;
    EXTI_InitStructure.EXTI_Trigger = trigger;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);    
}

/**********************************************************
 * 嵌套向量中断控制
 * @prioritygroup:  优先级分组,如NVIC_PriorityGroup_1
 * @irqn:           中断号,如EXTI9_5_IRQn
 * @preemption:     抢占优先级,如0
 * @sub:            响应优先级(副优先级),如1
 *********************************************************/
static bool s_nvicConfig = false;
void stm32_nvic_init(uint32 prioritygroup,uint8 irqn, uint8 preemption,uint8 sub)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    if(!s_nvicConfig)
    {
        s_nvicConfig = true;
        NVIC_PriorityGroupConfig(prioritygroup);/* 设置优先级分组位(只能设置一次) */
    }
    NVIC_InitStructure.NVIC_IRQChannel = irqn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = preemption;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = sub;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**********************************************************
 * 串口初始化函数
 * @usart:      串口,如USART1
 * @baud:       波特率,如9600,115200
 * @databits:   数据位,如USART_WordLength_8b
 * @stopbits:   停止位,如USART_StopBits_1
 * @parity:     奇偶校验位,如USART_Parity_No
 *********************************************************/
void stm32_usart_init(uint32 usart,uint32 baud,uint16 databits, uint16 stopbits,uint16 parity)
{
    USART_InitTypeDef USART_InitStructure;
    USART_StructInit(&USART_InitStructure);
    if(baud!=0)
    {
        USART_InitStructure.USART_BaudRate = baud;
        USART_InitStructure.USART_WordLength = databits;
        USART_InitStructure.USART_StopBits = stopbits;
        USART_InitStructure.USART_Parity = parity;
        USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; /* 使能接收和发送 */
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    }

    /* 配置串口 */
    USART_Init((USART_TypeDef *)usart, &USART_InitStructure);

    /* 开接收中断 */
    USART_ITConfig((USART_TypeDef *)usart, USART_IT_RXNE, ENABLE);

    /* 串口使能 */
    USART_Cmd((USART_TypeDef *)usart, ENABLE);

    /* 清空发送完成标志位,保证立即可以发送 */
    USART_ClearFlag((USART_TypeDef *)usart, USART_FLAG_TC);
}


/********************************** 
 * 进入待机模式
 **********************************/
void stm32_power_standby(void)
{
    /* 选择待机模式 */
    NVIC_SystemLPConfig(NVIC_LP_SLEEPDEEP, ENABLE);
    /* 使能PWR外设时钟 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    /* 使能唤醒管脚(WakeUp-PA0),因此进入待机模式之前一定要使能端口A的时钟 */
    PWR_WakeUpPinCmd(ENABLE);
    /* 进入待机模式 */
    PWR_EnterSTANDBYMode();

}

/*--------------------------------------------------------*
 *                   平台初始化                           *
 *--------------------------------------------------------*/
void platform_init(void)
{
    /* 默认系统配置函数 */
    SystemInit();
    #if 0 //重新配置系统时钟
    stm32_sysclk_init(RCC_SYSCLK_Div1, 
                      RCC_HCLK_Div1, 
                      RCC_HCLK_Div2, 
                      RCC_PLLSource_HSE_Div1, 
                      RCC_PLLMul_9);
    #endif

    /* 使能外设GPIOA,GPIOB,GPIOC,GPIOD的时钟,否则外设无法正常工作 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO  |   /* IO复用时钟使能 */
                           RCC_APB2Periph_GPIOA |   /* A口时钟使能 */
                           RCC_APB2Periph_GPIOB |   /* B口时钟使能 */
                           RCC_APB2Periph_GPIOC |   /* C口时钟使能 */
                           RCC_APB2Periph_GPIOD |   /* D口时钟使能 */
                           RCC_APB2Periph_USART1,   /* usart1时钟使能 */
                           ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);/* usart2时钟使能 */
                          
}

static void sample_interrupt(void)
{
    stm32_exti_init(EXTI_Line5,EXTI_Mode_Interrupt,EXTI_Trigger_Falling,GPIO_PortSourceGPIOC,GPIO_PinSource5);
    stm32_nvic_init(NVIC_PriorityGroup_1,EXTI9_5_IRQn,0,1);
    /* 除此之外,还需在stm32f10x_it.c中添加中断处理函数EXTI_9_5_IRQHandler,
     * 该中断入口函数已经在startup_stm32f10x_md.s启动文件中定义
     */
}

void mdelay(uint32 ms)
{
    while(ms--)
    {
        uint32 mips=CPU_MIPMS;
        while(mips--);
    }
}
void udelay(uint32 us)
{
    while(us--)
    {
        uint32 mips=CPU_MIPUS;
        while(mips--);
    }
}

