/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "gpio.h"
#if MCU_STC12LE5608
void gpio_set_mode_p0(uint8 pin,uint8 mode)
{
    switch(mode){
    case GPIO_MODE_IN_OUT:
        P0M0 = BITMAP_CLR(P0M0, pin);
        P0M1 = BITMAP_CLR(P0M1, pin);
        break;
    case GPIO_MODE_OUT_PP:
        P0M0 = BITMAP_CLR(P0M0, pin);
        P0M1 = BITMAP_SET(P0M1, pin);
        break;
    case GPIO_MODE_IN_HR:
        P0M0 = BITMAP_SET(P0M0, pin);
        P0M1 = BITMAP_CLR(P0M1, pin);
        break;
    case GPIO_MODE_OUT_OD:
        P0M0 = BITMAP_SET(P0M0, pin);
        P0M1 = BITMAP_SET(P0M1, pin);
        break;
    default:
        break;
    }
}
void gpio_set_mode_p1(uint8 pin,uint8 mode)
{
    switch(mode){
    case GPIO_MODE_IN_OUT:
        P1M0 = BITMAP_CLR(P1M0, pin);
        P1M1 = BITMAP_CLR(P1M1, pin);
        break;
    case GPIO_MODE_OUT_PP:
        P1M0 = BITMAP_CLR(P1M0, pin);
        P1M1 = BITMAP_SET(P1M1, pin);
        break;
    case GPIO_MODE_IN_HR:
        P1M0 = BITMAP_SET(P1M0, pin);
        P1M1 = BITMAP_CLR(P1M1, pin);
        break;
    case GPIO_MODE_OUT_OD:
        P1M0 = BITMAP_SET(P1M0, pin);
        P1M1 = BITMAP_SET(P1M1, pin);
        break;
    default:
        break;
    }
}
#endif

