/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __TRACE_H__
#define __TRACE_H__

#include "platform.h"

#define DEBUG_VERSION       1   /* 定义是否打印debug信息 */
#define USE_PRINTF_FUNCTION 0   /* 定义是否使用标准库提供的printf函数 */
#define NEW_LINE_SIGN       "\r\n"


/********************************************************************************
 * 注意:C51不支持可变参数宏定义,即下面这种宏定义
 * #define TRACE_DBG(fmt,arg...) printf(fmt,##arg)
 * 所以这里打印语句只能有一个参数!!!
 */
#if USE_PRINTF_FUNCTION
    #include <stdio.h>
    #if DEBUG_VERSION
    #define TRACE_DBG   printf
    #else
    #define TRACE_DBG(str)
    #endif
	#define TRACE_REL   printf
    #define TRACE_ERR   printf		  
    #define TRACE_HEX   printf
    #define TRACE_DEC   printf
    #define TRACE_CHR   printf
    
#else                   /* 不使用printf函数 */
    #include "uart.h"
    #define UART_PRINT_DEV   UART_COM0
    
    #if DEBUG_VERSION
    #define TRACE_DBG(str)   {uart_send_str(UART_PRINT_DEV,"[DBG]");uart_send_str(UART_PRINT_DEV,str);uart_send_str(UART_PRINT_DEV,NEW_LINE_SIGN);}
    #else
    #define TRACE_DBG(str)
    #endif
    
    #define TRACE_REL(str)   {uart_send_str(UART_PRINT_DEV,"[REL]");uart_send_str(UART_PRINT_DEV,str);uart_send_str(UART_PRINT_DEV,NEW_LINE_SIGN);}
    #define TRACE_ERR(str)   {uart_send_str(UART_PRINT_DEV,"[ERR]");uart_send_str(UART_PRINT_DEV,str);uart_send_str(UART_PRINT_DEV,NEW_LINE_SIGN);}
    #define TRACE_HEX(bstr,an_uint8,estr)   {uart_send_str(UART_PRINT_DEV,bstr);\
                                          uart_format_value(UART_PRINT_DEV,an_uint8,16);\
                                          uart_send_str(UART_PRINT_DEV,estr);}
    #define TRACE_DEC(bstr,an_uint8,estr)   {uart_send_str(UART_PRINT_DEV,bstr);\
                                          uart_format_value(UART_PRINT_DEV,an_uint8,10);\
                                          uart_send_str(UART_PRINT_DEV,estr);}
#endif

/********************************************************************************/
#if DEBUG_VERSION
    #define my_assert_param(expr) ((expr) ? (void)0 : my_assert_failed(__FILE__, __LINE__))
    void my_assert_failed(char* file, uint32 line);
	#define ASSERT(cond)    my_assert_param(cond)
#else
	#define ASSERT(cond)
#endif
/********************************************************************************/

#endif
