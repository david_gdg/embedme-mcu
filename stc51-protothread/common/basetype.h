/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __BASETYPE_H__
#define __BASETYPE_H__

#include "config.h"

/* Keil C51中enum类型为1字节 */
typedef unsigned char   uint8;
typedef signed char     sint8;
typedef unsigned int    uint16;
typedef signed int      sint16;
typedef unsigned int    uint32;
typedef signed int      sint32;

/* 定义bool类型 */
typedef char bool;
enum
{
	false=0,
	true=!false
};
enum
{
    API_OK          =  0,
    API_ERR         = -1,
    API_TIMEOUT     = -2,
    API_REBOOT      = -3,
    API_RESET       = -4,
    API_FULL        = -5,
    API_NULL        = -6,
};

/* 定义常用宏 */
#define UNUSED_PARAM(x) {x=x;}
#define TO_STR(x)       #x
#define CAT_STR(x,y)    x##y
#define DIGIT2ASCII(x)  (((x)>=0x0A)?((x)-0x0A+'A'):((x)+'0'))
#define ASCII2DIGIT(x)  (((x)>='a')?((x)-'a'+0x0A):(((x)>='A')?((x)-'A'+0x0A):((x)-'0')))

#define MAX(a,b)        (((a)>(b))?(a):(b))
#define MIN(a,b)        (((a)<(b))?(a):(b))
#define CLIP(min,x,max) (((x)>(max))?(max):(((x)<(min))?(min):(x)))

#define BITMAP_GET(value,bit)  (!!((1u<<(bit))&(value)))   /**< 获取value的第bit位的值 */
#define BITMAP_SET(value,bit)  ((1u<<(bit))|(value))       /**< 设置value的第bit位的值 */
#define BITMAP_CLR(value,bit)  ((~(1u<<(bit)))&(value))    /**< 清除value的第bit位的值 */


/* 当condition为true,则忙等,直至timeout */
#define WAIT_ON(condition,timeout)  {sint32 i = timeout;\
                                   while(condition){i--;if(i<=0){goto TIMEOUT;}}}

#endif

