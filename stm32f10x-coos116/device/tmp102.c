/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include "tmp102.h"
#include "i2c.h"

#define TMP102_ADDR         0x90    /* I2C�豸��ַ */
#define TMP102_REG_TEMP     0x00    /* �¶�ֵ�Ĵ��� */
#define TMP102_REG_CONF     0x01    /* ���üĴ��� */
#define TMP102_REG_TLOW     0x02    /* ����ֵ�Ĵ��� */
#define TMP102_REG_THIGH    0x03    /* ����ֵ�Ĵ��� */

/* ��ֵת�¶�(ֻ��ȷ��1��:0x320->50C,0xC90->-55C) */
static sint8 tmp102_digit2temp(sint16 digit)
{
    sint8 temp;
    if (digit<0x800)
    {
        temp = (sint8)(digit>>4);
    }
    else
    {
        temp = (sint8)(0-((0x1000-digit)>>4));
    }
    return temp;
}

/* �¶�ת��ֵ(��:+50C->0x320,-55C->0xC90) */
static sint16 tmp102_temp2digit(sint8 temp)
{
    sint16 digit;
    if (temp>=0)
    {
        digit = temp;
        digit <<= 4 ;
    }
    else
    {
         temp = 0-temp;
         digit = 0x1000-(temp<<4);
    }
    return digit;
}


bool tmp102_init(sint8 lowTemp, sint8 highTemp)
{
    sint16 digit;
    uint8 val[3];

    /* д���üĴ��� */
    val[0] = TMP102_REG_CONF;
    val[1] = 0x62;
    val[2] = 0xa0;
    if(I2C_WRITE(I2C_BUS_0,TMP102_ADDR,val,sizeof(val))!=sizeof(val))
    {
        TRACE_ERR("tmp102 write REG01 error.");
        return false;
    }

    /* д���¼Ĵ��� */
    val[0] = TMP102_REG_TLOW;
    digit  = tmp102_temp2digit(lowTemp);
    val[1] = (uint8)(digit>>4);
    val[2] = (uint8)((digit&0x00F)<<4);
    if(I2C_WRITE(I2C_BUS_0,TMP102_ADDR,val,sizeof(val))!=sizeof(val))
    {
        TRACE_ERR("tmp102 write REG02 error.");
        return false;
    }
     /* д���¼Ĵ��� */
    val[0] = TMP102_REG_THIGH;
    digit  = tmp102_temp2digit(highTemp);
    val[1] = (uint8)(digit>>4);
    val[2] = (uint8)((digit&0x00F)<<4);
    if(I2C_WRITE(I2C_BUS_0,TMP102_ADDR,val,sizeof(val))!=sizeof(val))
    {
        TRACE_ERR("tmp102 write REG03 error.");
        return false;
    }
    return true;
}

bool tmp102_read_temperature(sint8* temp)
{
    uint8 reg = TMP102_REG_TEMP;
    uint8 digital[2]={0};
    if (I2C_READ(I2C_BUS_0,TMP102_ADDR,&reg, 1,digital,2)==2)
    {
        sint16 val  = digital[0];
        val <<=4;
        val += (digital[1]>>4);
        *temp = tmp102_digit2temp(val);
        //TRACE_REL("digital[0]=0X%02X,[1]=0X%02X",digital[0],digital[1]);
        return true;    
    }
    return false;
}

