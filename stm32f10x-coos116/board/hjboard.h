/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __HJBOARD_H__
#define __HJBOARD_H__
/**********************************************************
 File   : hjboard.h
 Bref   : 慧净HL_1C开发板头文件(金钻美STM32开发板转慧静51开发板)
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/
#include "gpio.h"

#define P0_0    PC(13)
#define P0_1    PB(9)
#define P0_2    PB(8)
#define P0_3    PB(7)
#define P0_4    PB(6)
#define P0_5    PB(5)
#define P0_6    PB(4)
#define P0_7    PB(3)

#define P1_0    PC(0)
#define P1_1    PC(1)
#define P1_2    PC(2)
#define P1_3    PC(3)
#define P1_4    PA(0)
#define P1_5    PA(1)
#define P1_6    PA(2)
#define P1_7    PA(3)

#define P2_0    PB(12)
#define P2_1    PB(13)
#define P2_2    PB(14)
#define P2_3    PB(15)
#define P2_4    PC(6)
#define P2_5    PC(7)
#define P2_6    PC(8)
#define P2_7    PC(9)

#define P3_0    PA(5)
#define P3_1    PA(6)
#define P3_2    PA(7)
#define P3_3    PC(4)
#define P3_4    PC(5)
#define P3_5    PB(0)
#define P3_6    PB(1)
#define P3_7    PB(2)

/* 独立键盘IO口 */
#define KEY_K1      (P3_4)
#define KEY_K2      (P3_5)
#define KEY_K3      (P3_6)
#define KEY_K4      (P3_7)

/*---------------------------------------------
               4X4矩阵键盘
  P3.4 P3.5 P3.6 P3.7
  |    |    |    |
  S1---S2---S3---S4----P3.0
  |    |    |    |
  S5---S6---S7---S8----P3.1
  |    |    |    |
  S9---S10--S11--S12---P3.2
  |    |    |    |
  S13--S14--S15--S16---P3.3
  --------------------------------------------*/
#define KSCAN_X0        (P3_4)      /* 水平0 */
#define KSCAN_X1        (P3_5)      /* 水平1 */
#define KSCAN_X2        (P3_6)      /* 水平2 */
#define KSCAN_X3        (P3_7)      /* 水平3 */
#define KSCAN_Y0        (P3_0)      /* 垂直0 */
#define KSCAN_Y1        (P3_1)      /* 垂直1 */
#define KSCAN_Y2        (P3_2)      /* 垂直2 */
#define KSCAN_Y3        (P3_3)      /* 垂直3 */

/* I2C */
#define I2C_SDA         (P2_0)
#define I2C_SCL         (P2_1)

/* DS1302 RTC */
#define RTC_SDIO        (P2_0)
#define RTC_SCLK        (P2_1)
#define RTC_RST         (P2_4)

#define GPIO_DB1        (P1_0)
#define GPIO_DB2        (P1_1)
#define GPIO_DB3        (P1_2)
#define GPIO_DB4        (P1_3)
#define GPIO_DB5        (P1_4)
#define GPIO_DB6        (P1_5)
#define GPIO_DB7        (P1_6)
#define GPIO_DB8        (P1_7)

#define GPIO_18B20      (P2_2)  /* 温度感应器 */
#define GPIO_FM         (P2_3)

#define GPIO_INT1       (P3_3)

/* 串口引脚配置 */
#define GPIO_UART1_TX   PA(9)
#define GPIO_UART1_RX   PA(10)
#define GPIO_UART2_TX   PA(2)
#define GPIO_UART2_RX   PA(3)
#define GPIO_UART3_TX   PB(10)
#define GPIO_UART3_RX   PB(11)

#define UART_MAX        1           /* 最大允许定义的串口数 */
#define COM_DEBUG       UART_COM0    /* 连接调试串口 */
#define COM_DEBUG_BAUD  115200

#define STM32_LED_D1    PD(2)
#define STM32_LED_D2    PA(8) 

typedef enum{
    GPIO_LCD_D0=0,
    GPIO_LCD_D1,
    GPIO_LCD_D2,
    GPIO_LCD_D3,
    GPIO_LCD_D4,
    GPIO_LCD_D5,
    GPIO_LCD_D6,
    GPIO_LCD_D7,
    GPIO_LCD_RS,
    GPIO_LCD_RW,
    GPIO_LCD_EN,
    GPIO_LED_L0,
    GPIO_LED_L1,
    GPIO_LED_L2,
    GPIO_LED_L3,
    GPIO_LED_L4,
    GPIO_LED_L5,
    GPIO_LED_L6,
    GPIO_LED_L7,
    GPIO_DIG_DU,/* 数码管段选信号 */
    GPIO_DIG_WE,/* 数码管位选信号 */
    GPIO_MAX
}GPIO_ID_E;

#endif
