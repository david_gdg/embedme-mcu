/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __TRACE_H__
#define __TRACE_H__
#include "board.h"
#include "basetype.h"
#include <string.h>
#include <stdio.h>

#define USE_TRACE           1

#define TRACE_LEVEL_DIS     0   /* 禁止打印信息 */
#define TRACE_LEVEL_REL     1   /* 发布版信息(必须打印) */
#define TRACE_LEVEL_ERR     2   /* 错误信息 */
#define TRACE_LEVEL_WAR     3   /* 警告信息 */
#define TRACE_LEVEL_DBG     4   /* 调试信息 */
#define TRACE_LEVEL_EXT     5   /* 附加调试信息 */

#if USE_TRACE
#define DEBUG_VERSION       1   /* 定义是否打印debug信息 */
#define USE_STD_PRINTF      0   /* 定义是否使用标准库提供的printf函数 */
#define NEW_LINE_SIGN       "\r\n"

/********************************************************************************/
#if USE_STD_PRINTF
    #define PRINTF  printf
    #if DEBUG_VERSION
    #define TRACE_DBG(fmt,arg...)   {PRINTF("\033[36m\033[1m[DBG]"fmt"\033[0m"NEW_LINE_SIGN,##arg);}
    #else
    #define TRACE_DBG(fmt,arg...)
    #endif
	#define TRACE_REL(fmt,arg...)   {PRINTF("\033[32m\033[1m[REL]"fmt"\033[0m"NEW_LINE_SIGN,##arg);}
    #define TRACE_WAR(fmt,arg...)   {PRINTF("\033[33m\033[1m[WAR]"fmt"\033[0m"NEW_LINE_SIGN,##arg);}
    #define TRACE_ERR(fmt,arg...)   {PRINTF("\033[31m\033[1m[ERR]"fmt"\033[0m"NEW_LINE_SIGN,##arg);}	
    #define TRACE_HEX(tag,buf,len)
    #define TRACE_DEC(tag,buf,len)
    #define TRACE_RED(fmt,arg...)    {PRINTF("\033[31m\033[1m"fmt"\033[0m"NEW_LINE_SIGN,##arg);}	
    #define TRACE_GREEN(fmt,arg...)  {PRINTF("\033[32m\033[1m"fmt"\033[0m"NEW_LINE_SIGN,##arg);}
    #define TRACE_YELLOW(fmt,arg...) {PRINTF("\033[33m\033[1m"fmt"\033[0m"NEW_LINE_SIGN,##arg);}
    #define TRACE_CYAN(fmt,arg...)   {PRINTF("\033[36m\033[1m"fmt"\033[0m"NEW_LINE_SIGN,##arg);}
    
#else  /* 不使用C标准库printf函数 */
    #include "uart.h"
    #include <stdio.h>
    #define UART_PRINT_DEV          COM_DEBUG
    #define TRACE_BUF_LEN           400
    int mcu_printf(const char* fmt,...);/* 针对单片机的简化版printf,仅支持%d,%x和%s!!! */
    extern char g_traceBuf[TRACE_BUF_LEN];
    extern sint8 g_traceLevel;
    void trace_printf(const char* fmt,...);
    #define TRACE_LEVEL(level) {g_traceLevel = level;}
    #define PRINTF(fmt,arg...) {if(g_traceLevel>TRACE_LEVEL_DIS){char* pstr=g_traceBuf;memset(pstr,0,TRACE_BUF_LEN);snprintf(pstr,TRACE_BUF_LEN-1,fmt,##arg);\
                                while(*(pstr)!=0){uart_send_byte(UART_PRINT_DEV,*(pstr));pstr++;}}}
    #if DEBUG_VERSION
    #define TRACE_DBG(fmt,arg...)   {if(g_traceLevel>=TRACE_LEVEL_DBG){PRINTF("\033[36m\033[1m[DBG]");\
                                     PRINTF(fmt,##arg);\
                                     PRINTF("\033[0m"NEW_LINE_SIGN);}}
    #else
    #define TRACE_DBG(fmt,arg...)
    #endif
    #define TRACE_REL(fmt,arg...)   {if(g_traceLevel>=TRACE_LEVEL_REL){PRINTF("\033[32m\033[1m[REL]");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    #define TRACE_WAR(fmt,arg...)   {if(g_traceLevel>=TRACE_LEVEL_WAR){PRINTF("\033[33m\033[1m[WAR]");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    #define TRACE_ERR(fmt,arg...)   {if(g_traceLevel>=TRACE_LEVEL_ERR){PRINTF("\033[31m\033[1m[ERR]");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    
    #define TRACE_RED(fmt,arg...)    {if(g_traceLevel>=TRACE_LEVEL_DIS){PRINTF("\033[31m\033[1m");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    #define TRACE_GREEN(fmt,arg...)  {if(g_traceLevel>=TRACE_LEVEL_DIS){PRINTF("\033[32m\033[1m");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    #define TRACE_YELLOW(fmt,arg...) {if(g_traceLevel>=TRACE_LEVEL_DIS){PRINTF("\033[33m\033[1m");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    #define TRACE_CYAN(fmt,arg...)   {if(g_traceLevel>=TRACE_LEVEL_DIS){PRINTF("\033[36m\033[1m");PRINTF(fmt,##arg);PRINTF("\033[0m"NEW_LINE_SIGN);}}
    
#endif
/* 下面两个宏中定义的循环变量名称要特殊一点(如_I_),防止被调用函数作用域的变量覆盖!!! */
#define TRACE_HEX(tag,buf,len)  {if(g_traceLevel>=TRACE_LEVEL_EXT){uint32 _I_;PRINTF("<HEX:%s>(%d)-[",tag,len);\
                                 for(_I_=0;_I_<len;_I_++){ \
                                 PRINTF("%02x ",*(buf+_I_));}\
                                 PRINTF("]"NEW_LINE_SIGN);}}
#define TRACE_DEC(tag,buf,len)  {if(g_traceLevel>=TRACE_LEVEL_EXT){uint32 _I_;PRINTF("<DEC:%s>(%d)-[",tag,len);\
                                 for(_I_=0;_I_<len;_I_++){ \
                                 PRINTF("%4d ",*(buf+_I_));}\
                                 PRINTF("]"NEW_LINE_SIGN);}}

/********************************************************************************/
#if DEBUG_VERSION
    #define mcu_assert_param(expr) ((expr) ? (void)0 : mcu_assert_failed(__FILE__, __LINE__))
    void mcu_assert_failed(char* file, uint32 line);
	#define ASSERT(cond)    my_assert_param(cond)
#else
	#define ASSERT(cond)
#endif
/********************************************************************************/
#else   /* USE_TRACE == 0 */
#define TRACE_DBG(fmt,arg...)
#define TRACE_REL(fmt,arg...)
#define TRACE_WAR(fmt,arg...)
#define TRACE_ERR(fmt,arg...)
#define TRACE_HEX(tag,buf,len)
#define TRACE_DEC(tag,buf,len)
#define TRACE_RED(fmt,arg...)
#define TRACE_GREEN(fmt,arg...)
#define TRACE_YELLOW(fmt,arg...)
#define TRACE_CYAN(fmt,arg...)
#endif

#endif
